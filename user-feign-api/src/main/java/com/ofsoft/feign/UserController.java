package com.ofsoft.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author OF
 * @version v1.0
 * @className UserController
 * @date 2019/3/26
 */
@RestController
public class UserController {

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/user")
    public String index(){
        return userService.index();
    }

}
