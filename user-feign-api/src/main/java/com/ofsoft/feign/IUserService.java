package com.ofsoft.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author OF
 * @version v1.0
 * @className IUserServer
 * @date 2019/4/4
 */
@FeignClient(value = "user-service", fallback = UserSericeHystrix.class)
public interface IUserService {
    @RequestMapping(value = "/user")
    public String index();
}
