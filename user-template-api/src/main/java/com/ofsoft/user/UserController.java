package com.ofsoft.user;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author OF
 * @version v1.0
 * @className UserController
 * @date 2019/3/26
 */
@RestController
@RefreshScope
public class UserController {
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/user")
    @HystrixCommand(fallbackMethod = "fallback")
    public String index() {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://user-service:8083/user/", String.class);
        return responseEntity.getBody();
    }

    public String fallback() {
        return "fallbck";
    }


  /*  @Value("${com.didispace.message}")*/
    private String message;
    @Value("${name}")
    private String name;

    @GetMapping("/test")
    public String test() {
        return message;
    }


 @GetMapping("/name")
    public String name() {
        return name;
    }

}
