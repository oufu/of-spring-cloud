package com.ofsoft.user;

import com.netflix.appinfo.InstanceInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author OF
 * @version v1.0
 * @className UserController
 * @date 2019/3/26
 */
@RestController
public class UserController {
    @Autowired
    private DiscoveryClient discoveryClient;
    @RequestMapping(value = "/user")
    public String index()   {
        List<ServiceInstance> serviceInstances =  discoveryClient.getInstances("user-service");
        serviceInstances.forEach(serviceInstance ->System.out.println(serviceInstance.getHost()+serviceInstance.getServiceId()) );
        return serviceInstances.get(0).getHost()+serviceInstances.get(0).getServiceId();
    }
}
